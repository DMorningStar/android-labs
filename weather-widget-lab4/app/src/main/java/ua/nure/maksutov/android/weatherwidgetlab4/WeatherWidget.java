package ua.nure.maksutov.android.weatherwidgetlab4;

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.AsyncTask;
import android.os.StrictMode;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.InetAddress;

import ua.nure.maksutov.android.weatherwidgetlab4.handler.HTTPDataHandler;
import ua.nure.maksutov.android.weatherwidgetlab4.location.FallbackLocationTracker;
import ua.nure.maksutov.android.weatherwidgetlab4.location.LocationTracker;

public class WeatherWidget extends AppWidgetProvider {
    // To identify the widget views click
    private static final String TEMPERATURE_CLICKED = "Temperature";
    private static final String HUMIDITY_CLICKED = "Humidity";

    // Construct the url to fetch weather JSON data from web
    private String mURLString =
            "http://api.openweathermap.org/data/2.5/weather?lat=%s&lon=%s&appid=5de00cdc4bb539cb448f6122b9cdc795";

    private LocationTracker locationTracker;

    public WeatherWidget() {
        super();
    }

    @Override
    public void onEnabled(Context context) {
        super.onEnabled(context);
        locationTracker = new FallbackLocationTracker(context);
        locationTracker.start();
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        /*
            AppWidgetManager
                Updates AppWidget state; gets information about installed AppWidget
                providers and other AppWidget related state.

            ComponentName
                Identifier for a specific application component (Activity, Service, BroadcastReceiver,
                or ContentProvider) that is available. Two pieces of information, encapsulated here,
                are required to identify a component: the package (a String) it exists in, and the
                class (a String) name inside of that package.

            RemoteViews
                A class that describes a view hierarchy that can be displayed in another process.
                The hierarchy is inflated from a layout resource file, and this class provides some
                basic operations for modifying the content of the inflated hierarchy.
        */
        ComponentName watchWidget = new ComponentName(context, WeatherWidget.class);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);

        Intent intent = new Intent(context, WeatherWidget.class);
        /*
            PendingIntent
                A description of an Intent and target action to perform with it. Instances of this
                class are created with getActivity(Context, int, Intent, int),
                getActivities(Context, int, Intent[], int), getBroadcast(Context, int, Intent, int),
                and getService(Context, int, Intent, int); the returned object can be handed to other
                applications so that they can perform the action you described on your behalf at a later time.
        */
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        remoteViews.setOnClickPendingIntent(R.id.tv_temperature, pendingIntent);

        remoteViews.setOnClickPendingIntent(
                R.id.tv_temperature,
                getPendingSelfIntent(context, TEMPERATURE_CLICKED)
        );

        remoteViews.setOnClickPendingIntent(
                R.id.tv_humidity,
                getPendingSelfIntent(context, HUMIDITY_CLICKED)
        );
        appWidgetManager.updateAppWidget(watchWidget, remoteViews);
    }

    // Catch the click on widget views
    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        //Toast.makeText(context, "Intent Received: "+action, Toast.LENGTH_SHORT).show();
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    @SuppressLint("DefaultLocale")
    @Override
    public void onReceive(Context context, Intent intent) {
        // Allow the network operation on main thread
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
        StrictMode.setThreadPolicy(policy);

        super.onReceive(context, intent);

        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
        RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
        ComponentName watchWidget = new ComponentName(context, WeatherWidget.class);

        Toast.makeText(context, "Requested", Toast.LENGTH_SHORT).show();

        // Check the internet connection availability
        if (isInternetConnected()) {
            Location location = locationTracker.getLocation();
            String lat = String.format("%.2f", location.getLatitude());
            String longitude = String.format("%.2f", location.getLongitude());
            Toast.makeText(context, "Fetching Data", Toast.LENGTH_SHORT).show();
            // Update the widget weather data
            // Execute the AsyncTask
            new ProcessJSONData(appWidgetManager, watchWidget, remoteViews).execute(String.format(mURLString, lat, longitude));
            remoteViews.setInt(R.id.tv_temperature, "setBackgroundResource", R.drawable.bg_green);
        } else {
            Toast.makeText(context, "No Internet", Toast.LENGTH_SHORT).show();
            remoteViews.setInt(R.id.tv_temperature, "setBackgroundResource", R.drawable.bg_red);
        }

//        if (TEMPERATURE_CLICKED.equals(intent.getAction())) {
//        }
//
//        if (HUMIDITY_CLICKED.equals(intent.getAction())) {
//        }

        appWidgetManager.updateAppWidget(watchWidget, remoteViews);
    }

    // AsyncTask to fetch, process and display weather data
    private class ProcessJSONData extends AsyncTask<String, Void, String> {
        private AppWidgetManager appWidgetManager;
        private ComponentName watchWidget;
        private RemoteViews remoteViews;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        public ProcessJSONData(AppWidgetManager appWidgetManager, ComponentName watchWidget, RemoteViews remoteViews) {
            // Do something
            this.appWidgetManager = appWidgetManager;
            this.watchWidget = watchWidget;
            this.remoteViews = remoteViews;
        }

        @Override
        protected String doInBackground(String... strings) {
            String stream;
            String urlString = strings[0];

            // Get jason data from web
            HTTPDataHandler hh = new HTTPDataHandler();
            stream = hh.GetHTTPData(urlString);

            // Return the data from specified url
            return stream;
        }

        protected void onPostExecute(String stream) {
            super.onPostExecute(stream);

            if (stream != null) {
                try {
                    JSONObject reader = new JSONObject(stream);
                    JSONObject coord = reader.getJSONObject("main");
                    String temperature = coord.getString("temp");
                    String humidity = coord.getString("humidity");
                    String city = reader.getString("name");
                    Double celsius = getCelsiusFromKelvin(temperature);
                    temperature = "" + celsius + " " + (char) 0x00B0 + "C";
                    Log.d("Temp", temperature);

                    remoteViews.setTextViewText(R.id.tv_temperature, temperature);
                    remoteViews.setTextViewText(R.id.tv_humidity, "H: " + humidity + " %");

                    appWidgetManager.updateAppWidget(watchWidget, remoteViews);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    // Method to get celsius value from kelvin
    public Double getCelsiusFromKelvin(String kelvinString) {
        Double kelvin = Double.parseDouble(kelvinString);
        Double numberToMinus = 273.15;
        Double celsius = kelvin - numberToMinus;
        // Rounding up the double value
        // Each zero (0) return 1 more precision
        // Precision means number of digits after dot
        celsius = (double) Math.round(celsius * 10) / 10;
        return celsius;
    }

    public Boolean isInternetConnected() {
        boolean status = false;
        try {
            InetAddress address = InetAddress.getByName("google.com");

            if (address != null) {
                status = true;
            }
        } catch (Exception e) // Catch the exception
        {
            e.printStackTrace();
        }
        return status;
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
        if(locationTracker != null) {
            locationTracker.stop();
            locationTracker = null;
        }
    }
}
