package ua.nure.maksutov.android.lab1.task2;

/**
 * Created by ledniov on 2014 November 08, 09:47.
 */
public class Calculator {
    private double operand;
    private double pendingOperand;
    private Operation pendingOperation;

    private enum Operation {
        ADD("+"), SUB("-"), MUL("*"), DIV("/"), CLEAR("C"), EQ("="), TOGGLE_SIGN("+/-");

        private String value;

        Operation(String value) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static Operation fromString(String operation) {
            for (Operation op : values()) {
                if (op.toString().equals(operation)) {
                    return op;
                }
            }

            throw new IllegalArgumentException("Illegal operation passed");
        }

    }

    public Calculator() {
        operand = 0;
        pendingOperand = 0;
        pendingOperation = null;
    }

    public void setOperand(double operand) {
        this.operand = operand;
    }

    public double getResult() {
        return operand;
    }

    public String toString() {
        return Double.toString(operand);
    }

    protected double performOperation(String selectedOperation) {
        Operation operation = Operation.fromString(selectedOperation);
        if (!operation.equals(Operation.EQ)) {
            pendingOperation = operation;
        }

        performPendingOperation();
        pendingOperand = operation.equals(Operation.EQ) ? 0 : operand;

        return operand;
    }

    protected void performPendingOperation() {
        switch (pendingOperation) {
            case ADD:
                operand = pendingOperand + operand;
                break;
            case SUB:
                if (pendingOperand != 0) {
                    operand = pendingOperand - operand;
                }
                break;
            case MUL:
                if (pendingOperand != 0) {
                    operand = pendingOperand * operand;
                } else {
                    pendingOperand = operand;
                }
                break;
            case DIV:
                if (operand != 0) {
                    operand = pendingOperand / operand;
                }
                break;
            case CLEAR:
                operand = 0;
                pendingOperation = null;
                break;
            case TOGGLE_SIGN:
                operand = (-1) * operand;
                break;
            default:
                operand = 0;
                pendingOperand = 0;
                break;
        }

    }
}
