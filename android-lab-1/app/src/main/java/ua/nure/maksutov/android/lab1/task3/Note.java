package ua.nure.maksutov.android.lab1.task3;

import android.content.ContentValues;
import android.util.Log;

import java.util.Date;

/**
 * Created by ledniov on 10/15/14.
 */
public class Note {

    public static final String KEY_ROWID = "_id";
    public static final String KEY_TITLE = "title";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_PRIORITY = "priority";
    public static final String KEY_DUE_DATE = "due_date";
    public static final String KEY_CREATED_AT = "created_at";
    public static final String KEY_FILE_PATH = "file_path";
    public static final String KEY_NOTIFICATION_SCHEDULED = "notification_scheduled";

    public static enum Priority {
        LOW, NORMAL, HIGH
    }

    private long id;
    private String title;
    private String description;
    private Priority priority;
    private Date dueDate;
    private Date createdAt;
    private String filePath;
    private boolean notificationScheduled;

    public Note() {
        createdAt = new Date();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public boolean isNotificationScheduled() {
        return notificationScheduled;
    }

    public void setNotificationScheduled(boolean value) {
        notificationScheduled = value;
    }

    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();

        Log.d("note", "content values due date - " + dueDate);

        values.put(KEY_TITLE, title);
        values.put(KEY_DESCRIPTION, description);
        values.put(KEY_PRIORITY, priority.toString());
        values.put(KEY_DUE_DATE, dueDate.getTime());
        values.put(KEY_CREATED_AT, createdAt.getTime());
        values.put(KEY_FILE_PATH, filePath);
        values.put(KEY_NOTIFICATION_SCHEDULED, notificationScheduled);

        return values;
    }

    @Override
    public String toString() {
        return "Note{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", priority=" + priority +
                ", dueDate=" + dueDate +
                ", createdAt=" + createdAt +
                ", filePath='" + filePath + '\'' +
                ", notificationScheduled=" + notificationScheduled +
                '}';
    }
}
