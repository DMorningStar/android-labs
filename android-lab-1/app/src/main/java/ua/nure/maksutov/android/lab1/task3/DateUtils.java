package ua.nure.maksutov.android.lab1.task3;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by ledniov on 15.11.14.
 */
public class DateUtils {

    public static String getTodayDate() {
        StringBuilder sb = new StringBuilder();
        Calendar c = GregorianCalendar.getInstance();

        sb.append(c.get(Calendar.YEAR)).append("-");
        sb.append(c.get(Calendar.MONTH) + 1).append("-");
        sb.append(c.get(Calendar.DAY_OF_MONTH));

        return sb.toString();
    }
}
