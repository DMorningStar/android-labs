package ua.nure.maksutov.android.lab1.task3;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import ua.nure.maksutov.android.lab1.R;


/**
 * TODO test image from sd card
 */
public class NoteEdit extends Activity {

    private static final String TAG = NoteEdit.class.toString();

    private static final int RESULT_LOAD_IMAGE = 101;

    private static final Map<Integer, Note.Priority> priorityMap = new HashMap<Integer, Note.Priority>();

    static {
        priorityMap.put(R.id.radioPriorityHigh, Note.Priority.HIGH);
        priorityMap.put(R.id.radioPriorityNormal, Note.Priority.NORMAL);
        priorityMap.put(R.id.radioPriorityLow, Note.Priority.LOW);
    }

    private EditText editTitle;
    private EditText editDescription;
    private DatePicker dueDate;
    private TimePicker dueTime;
    private RadioGroup radioGroup;
    private ImageView imageView;
    private NotesDbAdapter dbHelper;
    private Long rowId;

    private String imagePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeManager.setTheme(this);
        setContentView(R.layout.activity_note_edit);
        setTitle(R.string.edit_note);

        dbHelper = new NotesDbAdapter(this);
        dbHelper.open();

        initViewComponents();

        Note note = loadNoteFromExtras();
        if (note != null) {
            Log.d("extracted note", note.toString());
            fillViewComponents(note);
            Button btn = (Button) findViewById(R.id.buttonAddNote);
            btn.setText(R.string.buttonSaveNote);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dbHelper.close();
    }

    private void initViewComponents() {
        editTitle = (EditText) findViewById(R.id.editTitle);
        editDescription = (EditText) findViewById(R.id.editDescription);
        dueDate = (DatePicker) findViewById(R.id.dueDate);
        dueTime = (TimePicker) findViewById(R.id.dueTime);
        dueTime.setIs24HourView(true);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroupPriority);
        imageView = (ImageView) findViewById(R.id.edit_note_img);
    }

    private void fillViewComponents(Note note) {
        editTitle.setText(note.getTitle());
        editDescription.setText(note.getDescription());

        Log.d(TAG, "fillViewComponents, due date- " + note.getDueDate());

        Calendar c = GregorianCalendar.getInstance();
        c.setTime(note.getDueDate());

        dueDate.updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        dueTime.setCurrentHour(c.get(Calendar.HOUR_OF_DAY));
        dueTime.setCurrentMinute(c.get(Calendar.MINUTE));

        radioGroup.check(getRadioIdByPriority(note.getPriority()));

        String filePath = note.getFilePath();

        if (filePath != null) {
            imageView.setImageDrawable(Drawable.createFromPath(note.getFilePath()));
            imagePath = note.getFilePath();
        }

        rowId = note.getId();
    }

    private int getRadioIdByPriority(Note.Priority prio) {
        for (Map.Entry<Integer, Note.Priority> entry : priorityMap.entrySet()) {
            if (entry.getValue().equals(prio)) {
                return entry.getKey();
            }
        }

        return 0;
    }

    private Note loadNoteFromExtras() {
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            rowId = extras.getLong(Note.KEY_ROWID);
            if (rowId != null) {
                Cursor cursor = dbHelper.fetchNote(rowId);
                startManagingCursor(cursor);// TODO replace
                return NoteFactory.fromCursor(cursor);
            }
        }

        return null;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.note_edit, menu);
        return true;
    }

    public void selectImage(View view) {
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, RESULT_LOAD_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imagePath = cursor.getString(columnIndex);
            cursor.close();

            imageView.setImageDrawable(Drawable.createFromPath(imagePath));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void addNote(View view) {
        Log.d(TAG, "add note button clicked");

        if (!isFormValid()) {
            return;
        }

        Bundle bundle = new Bundle();

        bundle.putString(Note.KEY_TITLE, editTitle.getText().toString());
        bundle.putString(Note.KEY_DESCRIPTION, editDescription.getText().toString());

        Note.Priority prio = priorityMap.get(radioGroup.getCheckedRadioButtonId());

        bundle.putString(Note.KEY_PRIORITY, prio.name());
        bundle.putLong(Note.KEY_DUE_DATE, getDueDateTimestamp());
        bundle.putLong(Note.KEY_CREATED_AT, new Date().getTime());
        bundle.putString(Note.KEY_FILE_PATH, imagePath);
        bundle.putBoolean(Note.KEY_NOTIFICATION_SCHEDULED, false);

        if (rowId != null) {
            Log.d(TAG, "row id is not empty");
            bundle.putLong(Note.KEY_ROWID, rowId);
        }

        Intent intent = new Intent();
        intent.putExtras(bundle);
        setResult(RESULT_OK, intent);
        finish();
    }

    private boolean isFormValid() {
        if (editTitle.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter title", Toast.LENGTH_LONG).show();
            return false;
        }

        if (editDescription.getText().toString().isEmpty()) {
            Toast.makeText(this, "Please enter description", Toast.LENGTH_LONG).show();
            return false;
        }

        if (radioGroup.getCheckedRadioButtonId() < 0) {
            Toast.makeText(this, "Please select priority", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }

    private long getDueDateTimestamp() {
        Calendar c = GregorianCalendar.getInstance();

        Log.d(TAG, "day of month - " + dueDate.getDayOfMonth());
        Log.d(TAG, "current hour - " + dueTime.getCurrentHour());

        c.set(Calendar.YEAR, dueDate.getYear());
        c.set(Calendar.MONTH, dueDate.getMonth());
        c.set(Calendar.DAY_OF_MONTH, dueDate.getDayOfMonth());
        c.set(Calendar.HOUR_OF_DAY, dueTime.getCurrentHour());
        c.set(Calendar.MINUTE, dueTime.getCurrentMinute());
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTimeInMillis();
    }

}