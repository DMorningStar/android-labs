package ua.nure.maksutov.android.lab1.task3;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import ua.nure.maksutov.android.lab1.R;

/**
 * Created by ledniov on 19.10.14.
 */
public class NotesCursorAdapter extends CursorAdapter {

    private static final String TAG = NotesCursorAdapter.class.getSimpleName();
    private int layout;

    private final LayoutInflater inflater;
    private final Context context;

    private final Map<Note.Priority, Integer> priorityIcons = new HashMap<Note.Priority, Integer>();

    public NotesCursorAdapter(Context context, int layout, Cursor c, int flags) {
        super(context, c, flags);
        this.layout = layout;
        this.context = context;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        initPriorityIcons();
    }

    private void initPriorityIcons() {
        String currentTheme = ThemeManager.getCurrentThemeName(context);

        if (currentTheme.contains("Light")) {
            priorityIcons.put(Note.Priority.HIGH, R.drawable.ic_action_important_dark);
            priorityIcons.put(Note.Priority.NORMAL, R.drawable.ic_action_half_important_dark);
            priorityIcons.put(Note.Priority.LOW, R.drawable.ic_action_not_important_dark);
        } else {
            priorityIcons.put(Note.Priority.HIGH, R.drawable.ic_action_important);
            priorityIcons.put(Note.Priority.NORMAL, R.drawable.ic_action_half_important);
            priorityIcons.put(Note.Priority.LOW, R.drawable.ic_action_not_important);

        }
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return inflater.inflate(layout, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView titleTextView = (TextView) view.findViewById(R.id.noteTitle);
        TextView dateTextView = (TextView) view.findViewById(R.id.noteDate);
        ImageView imageView = (ImageView) view.findViewById(R.id.priorityIcon);

        Note note = NoteFactory.fromCursor(cursor);

        titleTextView.setText(note.getTitle());

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dueDate = df.format(note.getDueDate());
        dateTextView.setText(dueDate);

        imageView.setImageResource(priorityIcons.get(note.getPriority()));
    }

}
