package ua.nure.maksutov.android.lab1.task2;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.DecimalFormat;

import ua.nure.maksutov.android.lab1.R;

public class Task2Activity extends Activity implements View.OnClickListener {

    private static final String TAG = Task2Activity.class.getSimpleName();

    private Calculator calc;
    private TextView calcDisplay;
    private Boolean userIsTypingANumber = false;
    private static final String DIGITS = "0123456789.";

    DecimalFormat df = new DecimalFormat("@###########");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task2);
        setTitle(R.string.calculator);
        initComponents();
        initListeners();
    }

    private void initComponents() {
        calc = new Calculator();
        calcDisplay = (TextView) findViewById(R.id.calcDisplay);

        df.setMinimumFractionDigits(0);
        df.setMinimumIntegerDigits(1);
        df.setMaximumIntegerDigits(8);
    }

    private void initListeners() {
        findViewById(R.id.button0).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
        findViewById(R.id.button9).setOnClickListener(this);
        findViewById(R.id.buttonAdd).setOnClickListener(this);
        findViewById(R.id.buttonSubtract).setOnClickListener(this);
        findViewById(R.id.buttonMultiply).setOnClickListener(this);
        findViewById(R.id.buttonDivide).setOnClickListener(this);
        findViewById(R.id.buttonToggleSign).setOnClickListener(this);
        findViewById(R.id.buttonDecimalPoint).setOnClickListener(this);
        findViewById(R.id.buttonEquals).setOnClickListener(this);
        findViewById(R.id.buttonClear).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        String buttonPressed = ((Button) view).getText().toString();
        String calcText = calcDisplay.getText().toString();

        if (isDigitButtonPressed(buttonPressed)) {

            if (userIsTypingANumber) {
                if (!(buttonPressed.equals(".") && calcText.contains("."))) {
                    calcDisplay.append(buttonPressed);
                }
            } else {
                if (buttonPressed.equals(".")) {
                    calcDisplay.setText(0 + buttonPressed);
                } else {
                    calcDisplay.setText(buttonPressed);
                }
                userIsTypingANumber = true;
            }
        } else {
            if (userIsTypingANumber) {
                calc.setOperand(Double.parseDouble(calcText));
                userIsTypingANumber = false;
            }
            calc.performOperation(buttonPressed);
            calcDisplay.setText(df.format(calc.getResult()));
        }
    }

    private boolean isDigitButtonPressed(String buttonText) {
        return DIGITS.contains(buttonText);
    }

}
