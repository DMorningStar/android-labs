package ua.nure.maksutov.android.lab1.task3;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;

/**
 * Created by ledniov on 16.11.14 17:25.
 */
public class NotificationScheduler extends Thread {

    private static final String TAG = NotificationScheduler.class.getSimpleName();

    private final Context context;
    private final NotesDbAdapter dbAdapter;

    public NotificationScheduler(Context context, NotesDbAdapter dbAdapter) {
        this.context = context;
        this.dbAdapter = dbAdapter;
    }

    @Override
    public void run() {
        Cursor cursor = dbAdapter.fetchTodaysNotesWithoutScheduledNotification();

        if (cursor == null) {
            Log.d(TAG, "cursor is null");
            return;
        }

        while (cursor.moveToNext()) {
            Note note = NoteFactory.fromCursor(cursor);
            Log.d(TAG, "extracted note - " + note);
            schedule(note);
            note.setNotificationScheduled(true);
            dbAdapter.updateNote(note);
        }
    }

    private void schedule(Note note) {
        Log.d(TAG, "scheduling notification");

        Intent notificationIntent = new Intent(context, NotificationPublisher.class);
        notificationIntent.putExtra(Note.KEY_ROWID, note.getId());
        notificationIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Log.d(TAG, "notification time - " + note.getDueDate());

        long notificationTime = note.getDueDate().getTime();

        Log.d(TAG, "notification time timestamp - " + notificationTime);

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC, notificationTime, pendingIntent);
    }

}
