package ua.nure.maksutov.android.lab1.task3;

import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;

import java.util.Date;

/**
 * Created by ledniov on 2014 November 07, 22:50.
 */
public class NoteFactory {

    private static final String TAG = NoteFactory.class.getSimpleName();

    public static Note fromExtras(Bundle extras) {
        Note note = new Note();

        note.setId(extras.getLong(Note.KEY_ROWID, 0));
        note.setTitle(extras.getString(Note.KEY_TITLE));
        note.setDescription(extras.getString(Note.KEY_DESCRIPTION));
        note.setPriority(Note.Priority.valueOf(extras.getString(Note.KEY_PRIORITY)));
        note.setDueDate(new Date(extras.getLong(Note.KEY_DUE_DATE)));
        note.setCreatedAt(new Date(extras.getLong(Note.KEY_CREATED_AT)));
        note.setFilePath(extras.getString(Note.KEY_FILE_PATH));
        note.setNotificationScheduled(extras.getBoolean(Note.KEY_NOTIFICATION_SCHEDULED));

        Log.d(TAG, "due date - " + note.getDueDate().getTime());

        return note;
    }

    public static Note fromCursor(Cursor cursor) {
        Note note = new Note();

        int idIndex = cursor.getColumnIndexOrThrow(Note.KEY_ROWID);
        int titleIndex = cursor.getColumnIndexOrThrow(Note.KEY_TITLE);
        int descriptionIndex = cursor.getColumnIndexOrThrow(Note.KEY_DESCRIPTION);
        int priorityIndex = cursor.getColumnIndexOrThrow(Note.KEY_PRIORITY);
        int dueDateIndex = cursor.getColumnIndexOrThrow(Note.KEY_DUE_DATE);
        int createdAtIndex = cursor.getColumnIndexOrThrow(Note.KEY_CREATED_AT);
        int filePathIndex = cursor.getColumnIndexOrThrow(Note.KEY_FILE_PATH);
        int notifScheduled = cursor.getColumnIndexOrThrow(Note.KEY_NOTIFICATION_SCHEDULED);

        Log.d(TAG, "due date from cursor - " + cursor.getLong(dueDateIndex));

        note.setId(cursor.getLong(idIndex));
        note.setTitle(cursor.getString(titleIndex));
        note.setDescription(cursor.getString(descriptionIndex));
        note.setPriority(Note.Priority.valueOf(cursor.getString(priorityIndex)));
        note.setDueDate(new Date(cursor.getLong(dueDateIndex)));
        note.setCreatedAt(new Date(cursor.getLong(createdAtIndex)));
        note.setFilePath(cursor.getString(filePathIndex));
        note.setNotificationScheduled(cursor.getInt(notifScheduled) == 1);

        Log.d("Note", note.toString());

        return note;
    }

}
