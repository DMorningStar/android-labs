package ua.nure.maksutov.android.lab1.task3;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import ua.nure.maksutov.android.lab1.R;

import static ua.nure.maksutov.android.lab1.task3.Note.KEY_ROWID;

/**
 * Created by ledniov on 16.11.14 17:13.
 */
public class NotificationPublisher extends BroadcastReceiver {

    private static final String TAG = NotificationPublisher.class.getSimpleName();

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive");

        NotificationManager notifManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        long noteId = intent.getLongExtra(KEY_ROWID, 0);

        Log.d(TAG, "note id - " + noteId);

        NotesDbAdapter adapter = new NotesDbAdapter(context);
        adapter.open();
        Note note = NoteFactory.fromCursor(adapter.fetchNote(noteId));
        adapter.close();

        Log.d(TAG, "extracted note in receiver - " + note);

        Intent newIntent = new Intent(context, NoteView.class);
        newIntent.putExtra(KEY_ROWID, note.getId());
        newIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, newIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new Notification.Builder(context)
                .setContentTitle(note.getTitle())
                .setContentText(note.getDescription())
                .setWhen(System.currentTimeMillis())
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_action_important)
                .getNotification();

        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notifManager.notify(0, notification);
    }
}
