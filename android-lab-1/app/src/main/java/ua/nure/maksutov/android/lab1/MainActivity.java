package ua.nure.maksutov.android.lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ua.nure.maksutov.android.lab1.R;
import ua.nure.maksutov.android.lab1.task1.Task1Activity;
import ua.nure.maksutov.android.lab1.task2.Task2Activity;
import ua.nure.maksutov.android.lab1.task3.Task3Activity;


public class MainActivity extends Activity {

    private static final String TAG = MainActivity.class.toString();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void startTask1Activity(View view) {
        customStartAcvity(Task1Activity.class);
    }

    public void startTask2Activity(View view) {
        customStartAcvity(Task2Activity.class);
    }

    public void startTask3Activity(View view) {
        customStartAcvity(Task3Activity.class);
    }

    private void customStartAcvity(Class clazz) {
        Log.d(TAG, "starting activity " + clazz.toString());
        Intent intent = new Intent(this, clazz);
        startActivity(intent);
    }
}
