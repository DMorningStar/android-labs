package ua.nure.maksutov.android.lab1.task3;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.List;

public class NotesDbAdapter {

    public static final String KEY_ROWID = "_id";

    private static final String TAG = NotesDbAdapter.class.toString();
    private DatabaseHelper dbHelper;
    private SQLiteDatabase db;

    private static final String DATABASE_NAME = "data";
    private static final String DATABASE_TABLE = "notes";
    private static final int DATABASE_VERSION = 2;

    /**
     * Database creation sql statement
     */
    private static final String DATABASE_CREATE =
            "create table notes (" +
                    "_id integer primary key autoincrement, " +
                    "title text not null, " +
                    "description text not null, " +
                    "priority text not null, " +
                    "due_date integer not null, " +
                    "file_path text, " +
                    "created_at integer not null, " +
                    "notification_scheduled integer not null, " +
                    "CHECK (priority IN (\"LOW\",\"NORMAL\",\"HIGH\" )) " +
                    ");";

    private static final String TABLE_DELETE = "DELETE FROM " + DATABASE_TABLE;
    private static final String TABLE_DROP = "DROP TABLE IF EXISTS " + DATABASE_TABLE;

    private static final String[] ALL_FIELDS = new String[]{Note.KEY_ROWID, Note.KEY_TITLE,
            Note.KEY_DESCRIPTION, Note.KEY_PRIORITY, Note.KEY_DUE_DATE, Note.KEY_CREATED_AT, Note.KEY_FILE_PATH, Note.KEY_NOTIFICATION_SCHEDULED};

    private final Context mCtx;

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            Log.d("DBHelper", "onCreate - creating db");
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL(TABLE_DROP);
            onCreate(db);
        }
    }

    /**
     * Constructor - takes the context to allow the database to be
     * opened/created
     *
     * @param ctx the Context within which to work
     */
    public NotesDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    /**
     * Removes all notes from database
     */
    public void clear() {
        db.execSQL(TABLE_DELETE);
    }

    /**
     * Open the notes database. If it cannot be opened, try to create a new
     * instance of the database. If it cannot be created, throw an exception to
     * signal the failure
     *
     * @return this (self reference, allowing this to be chained in an
     * initialization call)
     * @throws SQLException if the database could be neither opened or created
     */
    public NotesDbAdapter open() throws SQLException {
        dbHelper = new DatabaseHelper(mCtx);
        db = dbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        dbHelper.close();
    }


    /**
     * Create a new note using the title and body provided. If the note is
     * successfully created return the new rowId for that note, otherwise return
     * a -1 to indicate failure.
     *
     * @return rowId or -1 if failed
     */
    public long createNote(Note note) {
        return db.insert(DATABASE_TABLE, null, note.toContentValues());
    }

    /**
     * Delete the note with the given rowId
     *
     * @param rowId id of note to delete
     * @return true if deleted, false otherwise
     */
    public boolean deleteNote(long rowId) {
        return db.delete(DATABASE_TABLE, KEY_ROWID + "=" + rowId, null) > 0;
    }

    /**
     * Return a Cursor over the list of all notes in the database
     *
     * @return Cursor over all notes
     */
    public Cursor fetchAllNotes() {
        return query(ALL_FIELDS);
    }

    private Cursor query(String[] fieldsToSelect) {
        return query(fieldsToSelect, null);
    }

    private Cursor query(String[] fieldsToSelect, String criteria) {
        return db.query(DATABASE_TABLE, fieldsToSelect, criteria, null, null, null, null);
    }

    public Cursor fetchTodayNotes() {
        String criteria = " date(due_date/1000, 'unixepoch') = '" + DateUtils.getTodayDate() + "'";

        Log.d(TAG, "today notes criteria - " + criteria);
        return query(ALL_FIELDS, criteria);
    }

    public Cursor fetchTodaysNotesWithoutScheduledNotification() {
        String criteria = " notification_scheduled = 0 AND date(due_date/1000, 'unixepoch') = '" + DateUtils.getTodayDate() + "'";
        return query(ALL_FIELDS, criteria);
    }

    public Cursor fetchAllNotesContainingText(String text) {
        String criteria = Note.KEY_TITLE + " LIKE '%" + text + "%' OR " + Note.KEY_DESCRIPTION + " LIKE '%" + text + "%'";
        return query(ALL_FIELDS, criteria);
    }

    // TODO move join logic to another place
    // TODO create generic method for queries
    public Cursor fetchAllNotesWithPriority(List<Note.Priority> priorities) {
        if (priorities.size() == 0) {
            // TODO
        }

        StringBuilder sb = new StringBuilder();
        for (Note.Priority prio : priorities) {
            sb.append("\"").append(prio.name()).append("\",");
        }

        String prioList = sb.toString().substring(0, sb.toString().length() - 1);
        String criteria = Note.KEY_PRIORITY + " IN (" + prioList + ")";

        return query(ALL_FIELDS, criteria);
    }

    /**
     * Return a Cursor positioned at the note that matches the given rowId
     *
     * @param rowId id of note to retrieve
     * @return Cursor positioned to matching note, if found
     * @throws SQLException if note could not be found/retrieved
     */
    public Cursor fetchNote(long rowId) throws SQLException {
        Cursor cursor = db.query(true, DATABASE_TABLE, ALL_FIELDS, Note.KEY_ROWID + "=" + rowId, null, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }

        return cursor;
    }

    /**
     * Update the note using the details provided. The note to be updated is
     * specified using the rowId, and it is altered to use the title and body
     * values passed in
     *
     * @return true if the note was successfully updated, false otherwise
     */
    public boolean updateNote(Note note) {
        ContentValues values = note.toContentValues();
        return db.update(DATABASE_TABLE, values, KEY_ROWID + "=" + note.getId(), null) > 0;
    }
}