package ua.nure.maksutov.android.lab1.task3;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CursorAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SearchView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ua.nure.maksutov.android.lab1.R;

/**
 * TODO view item
 * TODO cleanup code
 * TODO fix filtering when searching items
 */
public class Task3Activity extends ActionBarActivity {

    private static final String TAG = Task3Activity.class.getSimpleName();

    private static final int FAKE_DELAY_IN_SECONDS = 1;

    public static final int INSERT_ID = R.id.action_new;
    public static final int ACTIVITY_EDIT = 1;

    private static final int EDIT_MENU = 1;
    private static final int DELETE_MENU = 2;
    private static final int ACTIVITY_CREATE = 3;

    private ProgressBar progressBar;
    private ListView listView;
    private SearchView searchView;

    private MenuItem highPrio;
    private MenuItem normalPrio;

    private MenuItem lowPrio;
    private NotesDbAdapter dbHelper;

    private class PriorityFilteringTask extends BaseAsyncTask<Void, Void, Cursor> {

        public PriorityFilteringTask(ProgressBar progressBar) {
            super(progressBar);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected Cursor doInBackground(Void... ts) {

            try {
                TimeUnit.SECONDS.sleep(FAKE_DELAY_IN_SECONDS);
            } catch (InterruptedException e) {
                Log.e(TAG, "Background task failed", e);
            }

            List<Note.Priority> priorities = getCurrentPriorities();
            Log.d(TAG, "priorities - " + priorities);

            Cursor result = null;

            // TODO fix prio filtering when using search
            if (priorities.size() > 0) {
                result = dbHelper.fetchAllNotesWithPriority(priorities);
            }

            return result;
        }

        @Override
        protected void onPostExecute(Cursor result) {
            super.onPostExecute(result);

            if (result == null) {
                // TODO fix this case
                listView.removeAllViews();
            } else {
                fillDataFromCursor(result);
            }

            hideProgressBar();
        }

    }

    private class NotesLoadingTask extends BaseAsyncTask<Void, Void, Cursor> {

        public NotesLoadingTask(ProgressBar progressBar) {
            super(progressBar);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected Cursor doInBackground(Void... voids) {
            try {
                TimeUnit.SECONDS.sleep(FAKE_DELAY_IN_SECONDS);
            } catch (InterruptedException e) {
                Log.e(TAG, "Background task failed", e);
            }

            Cursor cursor = dbHelper.fetchAllNotes();

            return cursor;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            fillDataFromCursor(cursor);
            hideProgressBar();
        }
    }

    private class NotesSearchTask extends BaseAsyncTask<String, Void, Cursor> {

        public NotesSearchTask(ProgressBar progressBar) {
            super(progressBar);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showProgressBar();
        }

        @Override
        protected Cursor doInBackground(String... texts) {

            try {
                TimeUnit.SECONDS.sleep(FAKE_DELAY_IN_SECONDS);
            } catch (InterruptedException e) {
                Log.e(TAG, "Background task failed", e);
            }

            String textToSearch = texts[0];
            Cursor notesContainingText = dbHelper.fetchAllNotesContainingText(textToSearch);

            int count = notesContainingText.getCount();
            Log.d(TAG, "notes count - " + count);

            return notesContainingText;
        }

        @Override
        protected void onPostExecute(Cursor cursor) {
            super.onPostExecute(cursor);
            fillDataFromCursor(cursor);
            hideProgressBar();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ThemeManager.setTheme(this);
        setContentView(R.layout.activity_task3);

        setTitle(R.string.notes_activity);
        initDb();
        initActivityComponents();
        registerForContextMenu(listView);
        fillData();

        new NotificationScheduler(this, dbHelper).start();
    }

    @Override
    protected void onDestroy() {
        Log.d(TAG, "onDestroy");
        super.onDestroy();
        dbHelper.close();
    }

    private void initDb() {
        dbHelper = new NotesDbAdapter(this);
        dbHelper.open();
//        dbHelper.clear();
    }

    private void initActivityComponents() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        listView = (ListView) findViewById(R.id.notesListView);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                Intent intent = new Intent(Task3Activity.this, NoteView.class);
                intent.putExtra(Note.KEY_ROWID, id);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "request code " + requestCode);
        Log.d(TAG, "result code " + resultCode);

        if (resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            if (requestCode == ACTIVITY_CREATE) {
                dbHelper.createNote(NoteFactory.fromExtras(extras));
                fillData();
                new NotificationScheduler(this, dbHelper).start();
            } else if (requestCode == ACTIVITY_EDIT) {
                Log.d(TAG, "edit activity");
                Log.d(TAG, NoteFactory.fromExtras(extras).toString());
                dbHelper.updateNote(NoteFactory.fromExtras(extras));
                fillData();
                new NotificationScheduler(this, dbHelper).start();
            }
        }
    }

    private void fillData() {
        Log.d(TAG, "fill data");
        new NotesLoadingTask(progressBar).execute();
    }

    private void fillDataFromCursor(Cursor cursor) {
        startManagingCursor(cursor); // TODO change with LoaderManger/CursorManager

        CursorAdapter adapter = new NotesCursorAdapter(this, R.layout.activity_task3_list_item, cursor, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        listView.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.add(0, EDIT_MENU, 0, R.string.crud_edit);
        menu.add(0, DELETE_MENU, 0, R.string.crud_delete);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(TAG, "context item selected - " + item.getItemId());
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        long noteId = info.id;
        Log.d(TAG, "note id - " + noteId);

        switch (item.getItemId()) {
            case DELETE_MENU:
                dbHelper.deleteNote(noteId);
                fillData();
                return true;
            case EDIT_MENU:
                editNote(noteId);
                fillData();
                return true;
        }

        return super.onContextItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task3, menu);
        boolean result = super.onCreateOptionsMenu(menu);

        highPrio = menu.findItem(R.id.menu_priority_high);
        normalPrio = menu.findItem(R.id.menu_priority_normal);
        lowPrio = menu.findItem(R.id.menu_priority_low);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                fillData();
                return true;
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(TAG, "onQueryTextSubmit");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                Log.d(TAG, "onQueryTextChange - " + s);

                if (!s.isEmpty()) {
                    new NotesSearchTask(progressBar).execute(s);
                    return true;
                }

                return false;
            }
        });

        return result;
    }

    public void priorityChecked(MenuItem menuItem) {
        Log.d(TAG, "priority checked");
        menuItem.setChecked(!menuItem.isChecked());
        new PriorityFilteringTask(progressBar).execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "item clicked - " + item.getItemId());

        switch (item.getItemId()) {
            case INSERT_ID:
                createNote();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private List<Note.Priority> getCurrentPriorities() {
        List<Note.Priority> currentPriorities = new ArrayList<Note.Priority>();

        if (highPrio.isChecked()) {
            currentPriorities.add(Note.Priority.HIGH);
        }

        if (normalPrio.isChecked()) {
            currentPriorities.add(Note.Priority.NORMAL);
        }

        if (lowPrio.isChecked()) {
            currentPriorities.add(Note.Priority.LOW);
        }

        return currentPriorities;
    }

    public void editNote(long id) {
        Intent intent = new Intent(this, NoteEdit.class);
        intent.putExtra(Note.KEY_ROWID, id);
        startActivityForResult(intent, ACTIVITY_EDIT);
    }

    public void createNote() {
        Log.d(TAG, "create note");
        Intent intent = new Intent(this, NoteEdit.class);
        startActivityForResult(intent, ACTIVITY_CREATE);
    }

    public void openSettings(MenuItem item) {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }
}
