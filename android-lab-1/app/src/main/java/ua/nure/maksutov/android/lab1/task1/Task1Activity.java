package ua.nure.maksutov.android.lab1.task1;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceView;
import android.widget.LinearLayout;
import android.widget.SeekBar;

import ua.nure.maksutov.android.lab1.R;

public class Task1Activity extends Activity {

    private int seekR, seekG, seekB;
    SeekBar seekRed, seekGreen, seekBlue;
    LinearLayout seekBarScreen;
    SurfaceView colorView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task1);
        setTitle(R.string.color_picker);
        seekBarScreen = (LinearLayout) findViewById(R.id.seekBarScreen);
        seekRed = (SeekBar) findViewById(R.id.seekRed);
        seekBlue = (SeekBar) findViewById(R.id.seekBlue);
        seekGreen = (SeekBar) findViewById(R.id.seekGreen);
        colorView = (SurfaceView) findViewById(R.id.colorView);
        updateBackground();
        seekRed.setOnSeekBarChangeListener(seekBarChangeListener);
        seekGreen.setOnSeekBarChangeListener(seekBarChangeListener);
        seekBlue.setOnSeekBarChangeListener(seekBarChangeListener);
    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener
            = new SeekBar.OnSeekBarChangeListener() {

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            updateBackground();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    private void updateBackground() {
        seekR = seekRed.getProgress();
        seekG = seekGreen.getProgress();
        seekB = seekBlue.getProgress();
        colorView.setBackgroundColor(
                0xff000000
                        + seekR * 0x10000
                        + seekG * 0x100
                        + seekB
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.task1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
