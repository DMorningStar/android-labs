package ua.nure.maksutov.android.lab1.task3;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import ua.nure.maksutov.android.lab1.R;

public class NoteView extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ThemeManager.setTheme(this);
        setContentView(R.layout.activity_note_view);
        setTitle(R.string.view_note);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            long noteId = extras.getLong(Note.KEY_ROWID);
            fillComponentsWithNoteData(noteId);
        }
    }

    private void fillComponentsWithNoteData(long noteId) {
        NotesDbAdapter dbHelper = new NotesDbAdapter(this);
        dbHelper.open();

        Cursor cursor = dbHelper.fetchNote(noteId);
        Note note = NoteFactory.fromCursor(cursor);
        cursor.close();
        dbHelper.close();

        ((TextView) findViewById(R.id.viewNoteTitle)).setText("Title - " + note.getTitle());
        ((TextView) findViewById(R.id.viewNoteDescription)).setText("Description - " + note.getDescription());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.note_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
